# piwigo-deployment

## GitLab CI

Инфраструктура в рамках пайплайна подготавливается через Terraform. [Инструкция](https://cloud.yandex.ru/ru/docs/tutorials/infrastructure-management/terraform-quickstart) по началу работы с Terraform от Yandex Cloud.

Для удобства ключевые джобы вынесены в отдельный [репозиторий](https://gitlab.com/conchis-group/gitlab-ci/terraform-ci). Включение джоб осуществляется посредством директивы `include`:

```yaml
include:
  - project: "conchis-group/gitlab-ci/terraform-ci"
    ref: "main"
    file:
      - "stages.yml"
      - "jobs.yml"
```

GitLab variables:

| Type     | Key                   | Description                                                                                                                                         |
|----------|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| File     | TF_VAR_ssh_key_path   | Путь до публичного ключа (id_rsa.pub) GitLab раннера                                                                                                |
| Variable | TF_VAR_yc_access_key  | Статический ключ доступа для сервисного аккаунта Yandex Cloud ([Документация](https://cloud.yandex.ru/ru/docs/iam/operations/sa/create-access-key)) |
| Variable | TF_VAR_yc_bucket_name | Имя s3 bucket`a для хранения state файла                                                                                                            |
| Variable | TF_VAR_yc_cloud_id    | Идентификатор облака ([Документация](https://cloud.yandex.ru/ru/docs/resource-manager/operations/cloud/get-id))                                     |
| Variable | TF_VAR_yc_folder_id   | Идентификатор каталога ([Документация](https://cloud.yandex.ru/ru/docs/resource-manager/operations/folder/get-id))                                  |
| Variable | TF_VAR_yc_token       | IAM-токен аккаунта ([Документация](https://cloud.yandex.ru/ru/docs/iam/operations/iam-token/create))                                                |

При "ручном" деплое следует подставить актуальные значения в `env.sh` файл (уникальные для каждого окружения):

```shell
export TF_VAR_yc_folder_id=''
export TF_VAR_yc_cloud_id=''
export TF_VAR_yc_token=''
export TF_VAR_yc_bucket_name=''
export TF_VAR_yc_access_key=''
export TF_VAR_yc_secret_key=''
```

Переопределения дефолтных значений teraform-переменных, описанных в variables.tf, осуществляется посредством переменных окружения. Более подробно об этом можно почитать [здесь](https://developer.hashicorp.com/terraform/cli/config/environment-variables).

## Ansible
- Ansible-роли вынесены в отдельные репозитории:
    - [PhpMyAdmin](https://gitlab.com/conchis-group/ansible-roles/ansible-role-phpmyadmin)
    - [MySQL](https://gitlab.com/conchis-group/ansible-roles/ansible-role-mysql)
    - [Prometheus](https://gitlab.com/conchis-group/ansible-roles/ansible-role-prometheus)
    - [node-exporter](https://gitlab.com/conchis-group/ansible-roles/ansible-role-node-exporter)
    - [nginx](https://gitlab.com/conchis-group/ansible-roles/ansible-role-nginx) (не доделан)

Include осуществляется посредством [ansible-galaxy](https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html) и файла `requirements.yml`:

```yaml
ansible-galaxy install -r requirements.yml
```

В рамках проекта реализован `dynamic inventory`. Для запуска ansible плейбука из корня репозитория:

```bash
cd ansible
ansible-playbook -i ./inventories/dev/ansible-inventory.py playbook.yml
```

