#!/bin/bash

BUCKET_NAME=$TF_VAR_yc_bucket_name
BACKUP_DIR="/opt/sql-dump"
TIMESTAMP=$(date + "%Y%m%d%H%M%S")

mkdir -p $BACKUP_DIR
mysqldump -u $DB_USER -p $DB_PASSWORD $DB_NAME > $BACKUP_DIR/$DB_NAME-$TIMESTAMP.sql
gzip $BACKUP_DIR/$DB_NAME-$TIMESTAMP.sql

aws --endpoint-url=https://storage.yandexcloud.net/ \
			s3 cp $BACKUP_DIR/$DB_NAME-$TIMESTAMP.sql.gz s3://$BUCKET_NAME/$DB_NAME-$TIMESTAMP.sql.gz   
