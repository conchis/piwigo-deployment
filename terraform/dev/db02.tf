resource "yandex_compute_instance" "db02" {
  name        = "db02"
  hostname    = "db02"
  platform_id = "standard-v1"
  zone        = var.yc_zone

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
    }
  }

  network_interface {
    subnet_id = "${module.vpc_module.vpc_subnet_id}"
    nat       = true
  }

  metadata = {
    ssh-keys = "{var.username}:${file(var.ssh_key_path)}"
  }

  labels = {
    "ansible_group" = "db"
    "mysql_server_id" = "2"
    "mysql_replication_role" = "slave"
    "mysql_replication_master" = yandex_compute_instance.db01.network_interface.0.ip_address
  }
}
