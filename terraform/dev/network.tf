module "vpc_module" {
  source                 = "../modules/vpc_module"
  vpc_network_name       = var.dev_vpc_network_name
  vpc_subnet_name        = var.dev_vpc_subnet_name
  vpc_subnet_zone        = var.dev_vpc_subnet_zone
  vpc_subnet_cidr_blocks = var.dev_vpc_subnet_cidr_blocks
}

