terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  required_version = ">= 0.13"
  
  #terraform init -backend-config=s3.tfbackend
  #backend "s3" {
  #  endpoints = { 
  #    s3 = "https://storage.yandexcloud.net"
  #  }
  #  bucket     = "yc-bucket-987"
  #  region     = "ru-central1"
  #  key        = "terraform-dev.tfstate"
  #  skip_region_validation      = true
  #  skip_credentials_validation = true
  #}

}


provider "yandex" {
  token                    = var.yc_token
  cloud_id                 = var.yc_cloud_id
  folder_id                = var.yc_folder_id
  zone                     = var.yc_zone
}
