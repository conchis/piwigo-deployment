terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

resource "yandex_vpc_network" "vpc-network" {
  name = var.vpc_network_name
}

resource "yandex_vpc_subnet" "vpc-subnet" {
  name           = var.vpc_subnet_name
  v4_cidr_blocks = var.vpc_subnet_cidr_blocks
  zone           = var.vpc_subnet_zone
  network_id     = yandex_vpc_network.vpc-network.id
}
