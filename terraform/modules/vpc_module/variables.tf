variable "vpc_network_name" {
  type = string
}

variable "vpc_subnet_name" {
  type = string
}

variable "vpc_subnet_zone" {
  type = string
}

variable "vpc_subnet_cidr_blocks" {
  type = list(string)
}
