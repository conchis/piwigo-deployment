variable "yc_folder_id" {
	description = "Folder id"
	type        = string
}

variable "username" {
        description = "User name"
        type        = string
}

variable "sa_key_path" {
	description = "path to key.l=json"
	type        = string
	default     = "./key.json"
}

variable "ssh_key_path" {
	description = "path to ssh pub key"
	type        = string
	default     = "~/.ssh/id_rsa.pub"
}

variable "yc_zone" {
	type        = string
	default     = "ru-central1-a"
}

variable "yc_bucket_name" {
	description = "name of yandex cloud backet"
	type        = string
}

variable "access_key" {
	type        = string
}

variable "secret_key" {
	type        = string
}

variable "yc_cloud_id" {
	type        = string
}

variable "yc_token" {
	type        = string
}

variable "image_id" {
	type        = string
	default     = "fd84ocs2qmrnto64cl6m"
}

variable "dev_vpc_network_name" {
	type        = string
	default     = "dev-network"
}

variable "dev_vpc_subnet_name" {
        type        = string
        default     = "dev-subnet"
}

variable "dev_vpc_subnet_zone" {
        type        = string
        default     = "ru-central1-a"
}


variable "dev_vpc_subnet_cidr_blocks" {
	type        = list(string)
	default     = ["10.2.0.0/16"]
}
