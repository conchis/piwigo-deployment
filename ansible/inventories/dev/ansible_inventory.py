#!/usr/bin/env python3

import json
import requests
import argparse
import urllib3
import os

class Inventory:
	def __init__(self, YC_IAM_TOKEN, YC_FOLDER_ID):
		self.__headers = {
			'Accept': 'application/json',
			'Concept-Type': 'application/json',
			'Authorization': f'Bearer {YC_IAM_TOKEN}'
		}

		self.__parameters = {
			'folderId': YC_FOLDER_ID
		}

		self.__groups = {
			'all': {
				'children': []
			},
			'ungrouped': {
				'hosts':[]
			},
			'_meta': {
				'hostvars': {}
			}
		}

	def create(self):
		instances = self.__get_json()
		for instance in instances["instances"]:
			self.__add_host(instance)
			self.__add_meta(instance)
		return json.dumps(self.__groups)

	def __get_json(self):
		urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
		return requests.get(
			url = 'https://compute.api.cloud.yandex.net/compute/v1/instances',
			headers = self.__headers,
			params = self.__parameters,
			verify = False
		).json()

	def __add_group(self, group):
		if group not in self.__groups:
			self.__groups[group] = {
				'hosts': []
			}
			self.__groups['all']['children'].append(group)

	def __add_host(self, instance):
		ip = instance['networkInterfaces'][0]['primaryV4Address']['oneToOneNat']['address']
		group = instance['labels']['ansible_group']
		if group is None:
			self.__groups['ungrouped']['hosts'].append(ip)
		else:
			self.__add_group(group)
			self.__groups[group]['hosts'].append(ip)

	def __add_meta(self, instance):
		ip = instance['networkInterfaces'][0]['primaryV4Address']['oneToOneNat']['address']
		for label in instance['labels'].keys():
			if ip not in self.__groups['_meta']['hostvars']:
				self.__groups['_meta']['hostvars'][ip] = {}
			self.__groups['_meta']['hostvars'][ip][label] = instance['labels'][label]


def main():
	YC_IAM_TOKEN = os.getenv('YC_IAM_TOKEN', None)
	YC_FOLDER_ID = os.getenv('TF_VAR_yc_folder_id', None)
	inventory = Inventory(YC_IAM_TOKEN, YC_FOLDER_ID)
	print(inventory.create())


if __name__ == '__main__':
	main()

